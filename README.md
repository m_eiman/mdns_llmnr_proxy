# mDNS <> LLMNR proxy

For some reason, Microsoft decided to stick with LLMNR in Windows even after
it was rejected by the IETF. Which means that typing "ping mymac" on a PC will
fail since the Mac is only listening to lookups over mDNS.

## Name lookup proxy

This proxy will listen for lookup requests on both LLMNR and mDNS, and when a lookup
on either system appears to be failing it will check the other system if the searched-for
name is resolvable there. If it is, the relevant data is forwarded.

Only one computer running this service is needed per network, it will answer requests
to and from any device.

## Requirements

pip3 install dnspython3


## References

mDNS is described in [http://www.ietf.org/rfc/rfc6762.txt](RFC 6762)

LLMNR is described in [https://tools.ietf.org/html/rfc4795](RFC 4785)

dnspython [http://www.dnspython.org/docs/1.12.0/](docs)
