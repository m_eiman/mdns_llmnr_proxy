#!/usr/bin/env python3

import socket
import struct
import datetime

import dns.message
import dns.flags
import dns.rcode
import dns.opcode
import dns.rdtypes.IN.A
import dns.rdatatype
import dns.rdataclass
import dns.rrset


# open a listening socket that we'll use to read LLMNR packets
MCAST_GRP = '224.0.0.252'
MCAST_PORT = 5355

listener = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
listener.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
listener.bind((MCAST_GRP, MCAST_PORT))
listener.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, struct.pack("4sl", socket.inet_aton(MCAST_GRP), socket.INADDR_ANY))

responder = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
responder.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
responder.bind(('', MCAST_PORT))

g_db = {}


class LocalName:
    @staticmethod
    def by_name(name):
        if not name in g_db.keys():
            g_db[name] = LocalName(name)
        res = g_db[name]
        res.num_requests += 1
        res.requested_at = datetime.datetime.now()
        return res
    
    def __init__(self, name):
        self.name = name
        self.resolved_at = datetime.datetime.fromtimestamp(0)
        self.requested_at = datetime.datetime.fromtimestamp(0)
        self.num_requests = 0
        self.num_resolutions = 0
        self.address = None
    
    def resolve(self):
        """Look up address"""
        # TODO don't look it up if we already know it?
        self.num_resolutions += 1
        try:
            # TODO manually do a mDNS lookup, so that we can set a timeout of 100ms
            self.address = socket.gethostbyname(self.name)
            self.resolved_at = datetime.datetime.now()
        except:
            print("Failed to resolve {}".format(self.name))
            self.address = None


while True:
    raw,source = listener.recvfrom(9194) # receive up to maximum size packet
    
    if len(raw) == 0:
        # socket closed(?)
        break
    
    print("Request from {}:{}".format(source[0], source[1]))
    try:
        msg = dns.message.from_wire(raw)
    except:
        # ignore malformed messages
        # TODO log non-DNS data
        continue
    
    received_at = datetime.datetime.now()
    
    # Sanity checking according to spec
    valid = True
    errors = []
    if msg.opcode() != dns.opcode.QUERY:
        errors.append("Invalid opcode: " + msg.opcode())
        valid = False
    
    if msg.flags & dns.flags.TC != 0:
        errors.append("TC flag not allowed")
        valid = False
    
    if msg.rcode() != dns.rcode.NOERROR:
        errors.append("Invalid rcode: " + msg.rcode())
        valid = False
    
    if len(msg.question) != 1:
        errors.append("Invalid QD count")
        valid = False
    
    if len(msg.answer) != 0:
        errors.append("Invalid AN count")
        valid = False
    
    if len(msg.authority) != 0:
        errors.append("Invalid AR count")
        valid = False
    
    if valid:
        rdata = msg.question[0]
        
        # TODO
        #if rdata.rdclass != dns.rdtypes.IN or rdata.rdtype != dns.rdtypes.IN.A:
        #    valid = False
        
        # TODO sanity check name: no dots etc
        
        if not valid:
            print(raw)
            print(msg.to_text())
            print(errors)
            print("Dropping invalid request")
        else:
            # TODO move lookup into a worker thread so we don't block
            print(rdata)
            
            local_name = rdata.name.to_text() + "local."
            
            entry = LocalName.by_name(local_name)
            try:
                entry.resolve()
            except Exception as e:
                print("Something failed: {}".format(e))
            
            if datetime.datetime.now() - received_at > datetime.timedelta(milliseconds=100):
                print("Took too long, not worth replying")
            elif entry.address != None:
                #print("Reply:")
                reply = dns.message.make_response(msg)
                #reply.set_rcode(dns.rcode.NOERROR)
                #reply.question.append(rdata)
                if rdata.rdtype == dns.rdatatype.A:
                    print("{}: {}".format(entry.name, entry.address))
                    
                    reply.answer.append(dns.rrset.from_text(
                        rdata.name,
                        30,
                        "IN",
                        "A",
                        entry.address
                    ))
                #print(reply)
                responder.sendto(reply.to_wire(), source)
            else:
                print("Couldn't resolve address")
    
    print()
